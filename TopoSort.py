# ATTRIBUTION:
# The MIT License (MIT)
# Copyright (c) 2014 Alexey Kachayev
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
# is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

GRAY = 0
BLACK = 1
errorNode = None
dot = ''


def topological2(dagraph):
    global dot
    orderedList = []
    startSet = set(dagraph)
    nodeState = {}

    dot = 'digraph X\r\n{\r\n'  # start the dot program

    def dfs(node):
        global dot
        global errorNode
        nodeState[node] = GRAY
        for k in dagraph.get(node, ()):
            dot += str(node) + ' -> ' + str(k) + '\r\n'  # add a dot node
            sk = nodeState.get(k, None)
            if sk == GRAY:  # visited once already, a cycle exists
                errorNode = str(k) + ", " + str(node)
                raise ValueError("DAG CYCLE")
            if sk == BLACK:
                continue
            startSet.discard(k)
            dfs(k)
        orderedList.append(node)  # lowest to highest
        nodeState[node] = BLACK

    while startSet:
        dfs(startSet.pop())

    dot += '}'  # finish the dot program
    print(dot)  # print out the dot program for pasting

    return orderedList


def topological3(dagraph):
    global dot
    orderedList = []
    nodeState = {}
    startSet = []

    # load the start set from the graph dictionary
    for graph_item in dagraph:
        startSet.append(graph_item)

    # start the dot program
    dot = 'digraph X\r\n{\r\n'

    def dfs(node):
        global dot
        global errorNode
        # add item to nodeState dictionary
        nodeState[node] = GRAY
        for k in dagraph.get(node, ()):
            # add a dot relation
            dot += str(node) + ' -> ' + str(k) + '\r\n'
            sk = nodeState.get(k, None)
            if sk == GRAY:  # visited once already, a cycle exists
                errorNode = str(k) + ", " + str(node)
                dot += ' [color=red]}'
                print(dot)
                raise ValueError("DAG CYCLE")
            if sk == BLACK:
                continue
            # remove current item from startset if it exists
            if startSet.__contains__(k):
                startSet.remove(k)
            dfs(k)
        orderedList.append(node)  # lowest to highest
        nodeState[node] = BLACK

    while startSet:
        dfs(startSet.pop())

    # finish the dot program and print it out for verification
    dot += '}'
    print(dot)

    # return the ordered list, dependencies first, lowest to highest
    return orderedList


###############################################################################
#                                                                             #
#  MAIN                                                                       #
#                                                                             #
###############################################################################


def main():
    dagGraph = {
        'A4': ['A3'],
        'B3': ['B2', 'B1'],
        'A3': ['A1', 'A2'],
        'A2': ['A1'],
        'C1': ['A4', 'B4'],
        'B4': ['A3', 'B3'],

#        'B2': ['B4'],  # uncomment to show what happens when cycle is introduced
    }
    try:
        #  print(topological2(dagGraph))
        print(topological3(dagGraph))
    except ValueError:
        print("circular reference with " + errorNode)


if __name__ == '__main__':
    main()
